
package arrayopperations;

/**
 *
 * @author Geanina Tambaliuc
 */
public class ArrayOpperations {

    public static int findSmallest(int[] myArray)
    {
        int smallest=myArray[0];
        for(int i=0;i<myArray.length;i++)
        {
            if(myArray[i]<smallest)
                smallest=myArray[i];
        }
        return smallest;
    }
    public static int findSum(int[] theArray)
    {
        int sum=0;
        for(int i=0;i<theArray.length;i++)
            sum=sum+theArray[i];
        return sum;
    }
    public static double findAverage(int[] myArray)
    {
        double sum=0;
        for(int k=0;k<myArray.length;k++)
            sum=sum+myArray[k];
        return sum/myArray.length;
    }
    public static int findLargest(int[] myArray)
    {
        int largest=myArray[0];
        for(int i=0;i<myArray.length;i++)
        {
            if(myArray[i]>largest)
                largest=myArray[i];
        }
        return largest;
    }
    public static int[] reverseArray(int[] myArray )
    {
        int[] revArray=new int[myArray.length];
        int i=0,j=myArray.length-1;
        while(i<myArray.length && j>=0)
        {
            revArray[i]=myArray[j];
            i++;
            j--;
        }
        return revArray;
    }
    public static double findLength(int[] myArray)
    {
        double result=0;
        for(int i=0;i<myArray.length;i++)
            result=result+(myArray[i]*myArray[i]);
        return Math.sqrt(result);
    }
    
    
}
