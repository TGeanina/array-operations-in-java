
package arrayopperations;

/**
 *
 * @author Geanina Tambaliuc
 */
public class ArrayOperationsTest {
    public static void main(String[] args) {
        int[] myArray={0,-100,23,44,11,25,31,148};
        System.out.println("Smallest element: "+ArrayOpperations.findSmallest(myArray));
        System.out.println("Largest: "+ArrayOpperations.findLargest(myArray));
        System.out.println("Sum: "+ArrayOpperations.findSum(myArray));
        System.out.println("Average: "+ArrayOpperations.findAverage(myArray));
        int[] myReverseArray=ArrayOpperations.reverseArray(myArray);
        System.out.print("Reverse Array: ");
        for(int i=0;i<myReverseArray.length;i++)
            System.out.print(myReverseArray[i]+" ");
        System.out.println();
        System.out.println("Length: "+ArrayOpperations.findLength(myArray));
    }
    
}
